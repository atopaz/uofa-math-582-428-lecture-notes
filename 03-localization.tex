\chapter{Primes and Localization}

Throughout this chapter, all rings are assumed to be commutative unless otherwise explicitly stated.

\section{Operations on Ideals}

Recall that an ideal $\afrak$ of a ring $A$ is just a submodule of $A$.
Explicitly, this means that $\afrak$ is closed under addition and that for all $a \in A$ one has $a \cdot \afrak \subset \afrak$.

Given two ideals $\afrak$ and $\bfrak$ in $A$, we can form:
$$ \afrak + \bfrak = \{a + b \ | \ a \in \afrak, \ b \in \bfrak \}. $$
$$ \afrak \cap \bfrak = \{a \ | \ a \in \afrak, \ a \in \bfrak \}. $$
$$ \afrak \cdot \bfrak = \{\sum_i a_i b_i \ | \ a_i \in \afrak, \ b_i \in \bfrak\}. $$

\exercise{One has $\afrak \cdot (\bfrak + \cfrak) = \afrak \cdot \bfrak + \afrak \cdot \cfrak$}.

\begin{lemma}
  Let $\afrak$ be an ideal of $A$.
  The following are equivalent:
  \begin{enumerate}
  \item $A = \afrak$.
  \item $1 \in \afrak$. 
  \item $\afrak \cap A^\times$ is nonempty.
  \end{enumerate}
\end{lemma}
\begin{proof}
  \exercise{Easy. See proof above.}
\end{proof}

\section{Maximal Ideals}

\begin{corollary}
  Let $k$ be a ring.
  The following are equivalent:
  \begin{enumerate}
  \item $k$ is a field
  \item the only two ideals in $k$ are $(0)$ and $(1)$.
  \end{enumerate}
\end{corollary}
\begin{proof}
  \exercise{Left as an exercise.}
\end{proof}

We say that a proper ideal $\mfrak$ of $A$ is \emph{maximal} if it is not contained in any other proper ideal of $A$.

\begin{theorem}
  Any proper ideal $\afrak$ of a ring $A$ is contained in at least one maximal ideal.
\end{theorem}
\begin{proof}
  By replacing $A$ with $A/\afrak$, it suffices to prove that any ring has at least one maximal ideal.
  To see this, use Zorn's lemma on the collection of proper ideals of $A$ ordered by inclusion.
  It suffices to show that inceasing chains have upper bounds.
  If $(\afrak_i)_i$ is such a chain, then the union
  $$ \bigcup_i \afrak_i $$
  is again an ideal.
  It is also proper by the characterization of proper ideals from the above lemma \todo{add ref}.
  The claim follows from Zorn's lemma.
\end{proof}

\begin{lemma}
  Let $\mfrak$ be an ideal of $A$.
  the following are equivalent:
  \begin{enumerate}
  \item $\mfrak$ is maximal.
  \item $A/\mfrak$ is a field.
  \end{enumerate}
\end{lemma}
\begin{proof}
  \exercise{This is a reformulation of the lemma \todo{add ref} above which characterizes fields in terms of ideals.}
\end{proof}

\begin{lemma}
  Let $A$ be a ring.
  Then one has
  $$ A^\times = A \smin \bigcup_\mfrak \mfrak $$
  where $\mfrak$ varies over all maximal ideals of $A$.
\end{lemma}
\begin{proof}
  Units are not contained in any maximal ideal, and any non-unit is contained in some maximal ideal.
\end{proof}

\section{Multiplicative sets and prime ideals}

Let $A$ be ring.
Recall that an element $a \in A$ is a \emph{zerodivisor} if there exists a nonzero element $b \in A$ such that $a \cdot b = 0$.
If not, we say that $a$ is a nonzerodivisor.

\begin{definition}
  A subset $S$ of $A$ is called \emph{multiplciative} provided that $1 \in S$ and for all $s,t \in S$, one has $s \cdot t \in S$.
  An ideal $\pfrak$ of $A$ is called \emph{prime} provided that $A \smin \pfrak$ is a multiplicative subset of $A$.
\exercise{An ideal $\pfrak$ of $A$ is prime (as defined above) if and only if $A/\pfrak$ is a domain, i.e. the only zerodivisor of $A/\pfrak$ is $0$.}
\end{definition}

\begin{lemma}
  Let $f : A \to B$ be a morhpism of rings.
  If $T$ is multiplicative in $B$ then $f^{-1}(T)$ is multiplicative in $A$.
  If $f$ is surjective, the converse holds as well.
\end{lemma}
\begin{proof}
  \todo{add proof.}
\end{proof}
Since any field is a domain, we obtain:
\begin{corollary}
  Any maximal ideal is prime.
\end{corollary}

\section{The Jacobson Radical}

The \emph{Jacobson radical} of a ring $A$, denoted $\Rad(A)$, is the intersection of all maximal ideals of $A$.

\begin{lemma}
  Let $a \in A$ and $u \in A^\times$ be given.
  One has $a \in \Rad(A)$ if and only if $u - a \cdot b \in A^\times$ for all $b \in A$.
\end{lemma}
\begin{proof}
  Firstly, suppose $a \in \Rad(A)$. 
  Take $b \in A$ and suppose $u-ab \notin A^\times$ for $u \in A^\times$. 
  This implies $(u-ab)$ is a proper ideal of $A$. 
  Thus, there exists a maximal ideal $\mfrak$ such that $u+ab\in \mfrak$. 
  Since $a \in \Rad(A)$, this means $a \in \mfrak$ and so $ab\in \mfrak$. 
  However, this would mean that $u \in \mfrak$ which is impossible since $\mfrak$ is maximal.

  Conversely, suppose $x \notin \Rad(A)$. 
  Then there exists a maximal ideal $\mfrak$ such that $x \notin \mfrak$.
  Observe that $(x)+\mfrak\supset \mfrak$. 
  This means $(x)+\mfrak=(1)$. 
  Hence, take $u \in (x)+\mfrak$. 
  This means $u=m+xy$ for some $m \in \mfrak$ and $y \in A$. 
  Hence $m=u-xy\in \mfrak$. 
  Thus $u-xy$ cannot be a unit. 
\end{proof}

\begin{definition}
  A ring $A$ is called \emph{local} provided that $A$ has a unique maximal ideal.
  In this case, we will often write $(A,\mfrak)$ to implicitly mean that $\mfrak$ is the unique maximal ideal of $A$.
  The Jacobson radical of a local ring is the unique maximal ideal of $A$.
\end{definition}

\begin{theorem}
  Let $A$ be a ring.
  The following are equivalent:
  \begin{enumerate}
  \item $A \smin A^\times$ is an ideal.
  \item $A$ is a local ring.
  \end{enumerate}
  If these equivalent conditions hold true, then $A \smin A^\times$ is the unique maximal ideal of $A$.
\end{theorem}
\begin{proof}
  If $A$ is local with maximal ideal $\mfrak$ then $A^\times = A \smin \mfrak$ by lemma \todo{ref}.
  Conversely, if $A \smin A^\times = \afrak$ is an ideal, is must be a proper ideal since it contains no unit, and thus it is contained in some maximal ideal.
  But lemma \todo{ref} ensures this is the unique maximal ideal and that it actually agrees with $\afrak$.
\end{proof}

\begin{example}
  \todo{Examples of local rings}.
\end{example}

\section{The Nilradical}

The \emph{nilradical} of $A$, denoted $\nrad{(0)}$, is the set
$$ \sqrt{(0)} = \{x \in A \ | \ \exists n \in \Zbf_{\geq 1}, \ x^n = 0\}. $$
More generally, for any ideal $\afrak$, we write
$$ \nrad{\afrak} = \{x \in A \ | \ \exists n \in \Zbf_{\geq 1}, \ x^n \in \afrak\}. $$

\begin{theorem}
  The ideal $\sqrt{(0)}$ is the intersection of all prime ideals.
\end{theorem}

Before we prove this, we need a useful lemma.
\begin{lemma}
  Let $S$ be a multiplicative subset and $\afrak$ an ideal which is disjoint from $S$.
  Consider the set $\Sscr$ of ideals containing $\afrak$ which are disjoint from $S$.
  Then $\Sscr$ has a maximal element $\pfrak$ and $\pfrak$ is prime.
\end{lemma}
\begin{proof}
  The set $\Sscr$ is nonempty by assumption, and \exercise{it's easy to check that this set has upper bounds of all chains.}
  Let $\pfrak$ be a maximal element (which exists by Zorn's lemma).
  We show that $\pfrak$ is prime.
  Let $a,b$ be in $A$ but not in $\pfrak$, so that $\pfrak + (a)$ and $\pfrak + (b)$ are ideals which are not contained in $\Sscr$ (by the maximality of $\pfrak$).
  Thus, there exist $p,q \in \pfrak$ and $x,y \in A$ such that $p + ax$, $q + by \in S$.
  Since $S$ is multiplicative, we have
  $$ (p + ax) \cdot (q + by) = pq + pby + qax + abxy \in S $$
  while the first three terms in the sum are elements of $\pfrak$.
  It follows that $xy \notin \pfrak$ for otherwise it would follow that this whole sum is an element of $S$ (which is disjoint from $\pfrak$).
\end{proof}

\begin{proof}[Proof of Theorem \todo{ref}]
  WLOG, we may \exercise{reduce to the case where $\afrak = (0)$ by replacing $A$ with $A/\afrak$.}
  If $x$ is nilpotent then $x \in \pfrak$ for every prime $\pfrak$ since $x$ must map to $0$ in $A/\pfrak$.
  Conversely, if $x$ is not nilpotent, consider the set $S = \{1,x,x^2,\ldots\}$.
  Note that $S$ is disjoint from $\sqrt{(0)}$, so the set $\Sscr$ in the proof of the previous lemma is nonempty.
  This set $\Sscr$ has a maximal element which is prime, which does not contain $x$.
  It follows that $x$ is not an element of the intersection of all prime ideals of $A$.
\end{proof}

\begin{definition}
  We say that $A$ is \emph{reduced} provided that $\sqrt{(0)} = 0$.
\end{definition}

\section{Minimal Primes}

\begin{definition}
  Let $\afrak$ be an ideal of $A$.
  A prime $\pfrak$ is called \emph{minimal over $\afrak$} if $\pfrak$ is minimal in the collection of prime ideals containign $\afrak$.
  We say that $\pfrak$ is minimal provided it is minimal over $(0)$.
\end{definition}

\begin{lemma}
  Let $\afrak$ be an ideal, and $\qfrak$ a prime containing $\afrak$.
  Then $\qfrak$ contains some minimal prime over $\afrak$.
\end{lemma}
\begin{proof}
  This follows by Zorn's lemma.
  Details are left as an \exercise{exercise.}
\end{proof}

\begin{corollary}
  The radical $\sqrt{\afrak}$ is the intersection of all minimal primes over $\afrak$.
\end{corollary}

\begin{corollary}
  Let $A$ be a ring.
  The following are equivalent:
  \begin{enumerate}
  \item $A$ is reduced and has a unique minimal prime.
  \item $A$ is a domain.
  \end{enumerate}
  If these equivalent conditions hold, then the unique minimal prime of $A$ is $(0)$.
\end{corollary}

\section{Expansion and Contraction}

Let $B$ be an $A$-algebra with structure map $f : A \to B$.
Given an ideal $\afrak$ of $A$, we write
$$ \afrak \cdot B $$
for the ideal of $B$ generated by $f(\afrak)$, and call it the \emph{expansion} of $\afrak$ to $B$.
Given an ideal $\bfrak$ of $B$ we write $\bfrak \cap A$ for the preimage of $\bfrak$ in $A$, and call it the \emph{contradction} of $\bfrak$ to $A$. 

\begin{lemma}
  In the above context, the following hold:
  \begin{enumerate}
  \item One has $\afrak \subset (\afrak \cdot B)$ and $(\bfrak \cap A) \cdot B \subset \bfrak$.
  \item If $\bfrak = \afrak \cdot B$, then $\bfrak \cap A$ is the largest ideal of $A$ with expansion equal to $\bfrak$.
  \item If two expansions have the same contractions, then they are equal.
  \end{enumerate}
\end{lemma}
\begin{proof}
  \exercise{Left as an exercise.}
\end{proof}

\begin{proposition}
  Let $A \to B$ be a morphism of rings.
  Let $\pfrak$ be a prime ideal of $B$, then the contraction $\pfrak \cap A$ is a prime ideal in $A$.
  The converse holds when $f$ is surjective.
\end{proposition}

\section{Idempotents}

An element $e \in A$ is called \emph{idempotent} provided that $e^2 = e$.
If $e$ is idempotent then $1-e$ is again idempotent since
$$ (1-e)^2 = 1-2e+e^2 = 1-2e+e = 1-e. $$
We call $1-e$ the \emph{complementary idempotent to $e$}.

The set $A \cdot e$ can be given the structure of a ring with multiplicative unit $e$.
Indeed, addition is given by
$$ ae + be = (a+b)e $$
and multiplciation is given by
$$ (ae) \cdot (be) = (ab) \cdot e^2 = (ab) \cdot e. $$

the main example comes from:
\begin{example}
  Let $A$ and $B$ be two rings.
  Then $(1,0) \in A \times B$ is idempotent with complementary idempotent $(0,1)$.
\end{example}

\begin{proposition}
  Let $A$ be a ring with complementary idempotents $e$ and $e' = 1-e$.
  Set $A' := A \cdot e$ and $A'' := A \cdot e'$.
  and consider the map
  $$ A \to A' \times A'' $$
  defined by
  $$ a \mapsto (a \cdot e, a \cdot e'). $$
  This is a ring isomorphism.
\end{proposition}
\begin{proof}
  Checking it is a homomorphism is easy.
  For surjectivity, note:
  $$ x \cdot e + x' \cdot e' \mapsto (xe,x'e') $$
  and for injectivity note that $(xe,xe') = 0$ if and only if
  $$ x = x(e+e') = xe + xe' = 0. $$
\end{proof}

\begin{theorem}[The Chinese Remainder Theorem]
  Let $A$ be a ring and let $\afrak_1,\ldots,\afrak_n$ be ideals in $A$ which are pairwise comaximal, i.e. $\afrak_i + \afrak_j = A$ for all $i \neq j$.
  Then the canonical map 
  $$ A/(\afrak_1 \cdots \afrak_n) \to \prod_i A/\afrak_i $$
  is an isomorphism.
\end{theorem}
\begin{proof}
  \exercise{Exercise.}
\end{proof}

\section{Nakayama's Lemma}

We start with a generalized form of the Cayley-Hamilton theorem from basic linear algebra.
Suppose that $A$ is a commutative ring, and that $M = (a_{ij})_{i,j}$ is a square $n \times n$ matrix with coefficients in $A$.
Let $\Ibf$ denote the indetity matrix and let $T$ be an indeterminant.
The characteristic polynomial of $M$ is the element of $A[T]$ defined as
$$ \det(T \cdot I - M). $$
It is a monic polynomial of degree $n$ of the form:
$$ P_M(T) := T^n + a_1 T^{n-1} + \cdots + a_n. $$
If all the coefficients of $M$, $a_{i,j}$, are elements of a fixed ideal $\afrak$, then one has $a_k \in \afrak^k$ as well.
Given any polynomial $P(T) \in A[T]$, we may evaluate $P(M)$ in the usual way.


\begin{theorem}
  Let $N$ be an $R$-module which is finitely-generated, and let $n_1,\ldots,n_n$ be generators of $N$.
  Let $\phi : N \to N$ be an endomoprhism, and write $\phi(n_i) = \sum_j a_{ij} n_j$.
  Consider the matrix $M = (a_{ij})$ formed in this way
  Then one has $P_M(\phi) = 0$ as an element of $\End(N)$.
\end{theorem}
\begin{proof}
  Let $\delta_{ij}$ denote the Kronecker delta function and $\mu_{a_{ij}}$ the multiplication by $a_{ij}$.
  Consider the matrix $\Delta := (\delta_{ij} \phi - \mu_{a_{ij}}))$ with entries in $A[\phi]$.
  Let $X = (n_j)$ be the column vector formed by the generators of $N$.
  We can calculate: $\Delta \cdot X = 0$.
  Multiplying by $\Gamma$, the adjugate matrix of $\Delta$, we get:
  $$ \Gamma \Delta X = 0 $$
  and
  $$ \Gamma \Delta = \det(\Delta) \cdot \Ibf $$
  So that $\det(\Delta) \cdot n_j = 0$ for all $j$.
  Since the $n_j$ generate $N$, it follows that $\det(\Delta) = 0$ as an endomorphism of $N$.
  We have $P_M(\phi) = \det(\Delta)$, so the assertion follows.
\end{proof}

\begin{corollary}
  Let $M$ be a finitely-generated $A$-module, and let $\afrak$ be an ideal of $M$.
  Then $M = \afrak \cdot M$ if and only if there exists some $a \in \afrak$ such that $(1+a) \cdot M = 0$.
\end{corollary}
\begin{proof}
  Let $m_1,\ldots,m_n$ generate $M$, and write $m_i = \sum_j a_{ij} m_j$ with $a_{ij} \in \afrak$.
  Consider the matrix $(a_{ij})$ with characteristic polynomial $P = T^n + a_1 T^{n-1} + \cdots + a_n$.
  Consider $a = a_1 + \cdots + a_n \in \afrak$.
  Then taking $\phi = \one_M$ in the previous theorem, we obtain $(1 + a) \cdot M = 0$.

  Conversely, if $a \in \afrak$ satisfies $(1+a) \cdot M = 0$ then $m = -a \cdot m$ for all $m \in M$ so that $M \subset \afrak \cdot M \subset M$ hence $M = \afrak \cdot M$.
\end{proof}

\subsection{Annihilators and Nakayama's Lemma}

Let $M$ be an $A$-module.
The \emph{annihilator of $M$} is the ideal
$$ \Ann(M) := \{x \in A \ | \ x \cdot m = 0 \forall m \in M \}. $$
The action of $A$ on $M$ descends to $A/\Ann(M)$.
The \emph{radical of $M$}, denoted $\Rad(M)$ is the intersection of all maximal ideals containing $\Ann(M)$.
Note that the image of $\Rad(M)$ in $A/\Ann(M)$ is the Jacobson radical of $A/\Ann(M)$.

\begin{lemma}
  If $x \in \Rad(M)$ and $m \in M$ satisfies $(1+x) \cdot m = 0$, then $m = 0$.
\end{lemma}
\begin{proof}
  By the observations above, we know that the image of $x$ in $A/\Ann(M)$ is contained in the Jacobson radical, hence the image of $1+x$ is a unit in $A/\Ann(M)$.
  Thus, we can find some $y \in A$ such that $1 - y \cdot (1+x) \in \Ann(M)$.
  But we have $(1+x) \cdot m = 0$ and $1-y \cdot (1+x)$ annihilates $M$ hence 
  $$ 0 = (1 - y \cdot (1+x)) \cdot m $$
  so that
  $$ m = y \cdot (1+x) \cdot m = y \cdot 0 = 0. $$
\end{proof}

\begin{theorem}[Nakayama's Lemma]
  Let $A$ be a commutative ring, $M$ a finitely-generated $A$-module, and $\mfrak$ an ideal contain in $\Rad(M)$.
  One has $M = \mfrak \cdot M$ if and only if $M = 0$.
\end{theorem}
\begin{proof}
  There exists some $a \in \mfrak$ with $(1+a) \cdot M = 0$, while $\mfrak \subset \Rad(M)$.
  The lemma above implies that $M = 0$.
\end{proof}

\begin{corollary}
  Suppose that $N \subset M$ is a submodule such that $M/N$ is finitely-generated.
  Assume $\mfrak \subset \Rad(M)$ is an ideal.
  If $N + \mfrak M = M$ then $N = M$.
\end{corollary}

\begin{corollary}
  Assume that $M$ is finitely-generated and $\mfrak \subset \Rad(M)$.
  Then $m_1,\ldots,m_n \in M$ generate $M$ if and only if the images of those elements generate $M/\mfrak \cdot M = (A/\mfrak) \otimes_A M$.
\end{corollary}

\begin{remark}
  Usually, we will use Nakayama's Lemma in the case where $A$ is a local ring and $\mfrak$ is the (unique) maximal ideal.
\end{remark}

\section{Modules over local rings}

Let $A$ be a commutative ring and $M$ an $A$-module.
Recall that $M$ is finitely-generated if it fits in an exact sequence of the form
$$ A^n \to M \to 0 $$
for some nonengative integer $n$.
We say that $M$ is called \emph{finitely-presented} provided that it fits in an exact sequence of the form
A module $M$ over a ring $A$ is called \emph{finitely-presented} provided that it fits in an exact sequence of the form
$$ A^m \to A^n \to M \to 0 $$
for some nonnegative integers $m$ and $n$.

\begin{lemma}
  Let
  $$ 0 \to L \to P \to M \to 0 $$
  and 
  $$ 0 \to L' \to P' \to M \to 0 $$
  be exact sequences with $P$ and $P'$ projective.
  Then $L \oplus P' \cong L' \oplus P$.
  \todo{Fix statement.}
\end{lemma}
\begin{proof}
  \todo{add proof}.
\end{proof}

\begin{lemma}
  Suppose that
  $$ 0 \to L \to A^n \to M \to 0 $$
  is exact and $M$ is finitely-presented.
  Then $L$ is finitely-generated.
\end{lemma}
\begin{proof}
  There is an exact sequence of the form
  $$ A^k \to A^l \to M \to 0 $$
  by assumption.
  Let $K$ be the image of $A^k \to A^l$.
  It suffices to note that $K \oplus A^n \cong L \oplus A^m$ by the lemma above, for then $L$ is a quotient of $A^{k + n}$.
\end{proof}

\begin{theorem}
  Let $A$ be a local ring with maximal ideal $\mfrak$, and let $M$ be a module over $A$.
  Then the following are equivalent:
  \begin{enumerate}
  \item $M$ is free of finite rank (i.e. $M \cong A^n$ for some nonnegative integer $n$).
  \item $M$ is projective and finitely-generated.
  \item $M$ is flat and finitely-presented.
  \end{enumerate}
\end{theorem}
\begin{proof}
  1 clearly implies 2 which implies that $M$ is flat and finitely-generated.
  Let us show that 2 further implies that $M$ is finitely-presented.
  Let $m_1,\ldots,m_n$ generate $M$, and consider the corresponding exact sequence
  $$ 0 \to K \to A^n \to M \to 0. $$
  As $M$ is projective, this splits, so there exists a surjection $A^n \to K \to 0$, hence $M$ is finitely-presented.

  Let us assume 3, and let $k = A/\mfrak$ denote the residue field of $A$.
  Consider the exact sequence 
  $$ 0 \to K \to A^n \to M \to 0 $$
  discussed above, and choose $n$ minimal.
  We know that the $m_1,\ldots,m_n$ generate $M$ if and only if their images generate $k \otimes_A M$ and since $n$ is minimal, these images form a basis of this $k$-vectorspace.
  But $M$ is flat, so we have an exact sequence
  $$ 0 \to k \otimes_A L \to k^n \to M \to 0 $$
  since $\Tor_1^A(k,M) = 0$, while the map $k^n \to M$ is an isomorphism by standard linear algebra over a field.
  It follows that $k \otimes_A L = 0$ hence $L = 0$ by Nakayama's Lemma.
\end{proof}

\section{Localization}

Let $A$ be a ring and $S$ a multiplicative subset of $A$.
We will define $S^{-1} A$ to be the initial $A$-algebra in which all elements of $S$ become units.
Precisely, $S^{-1}A$ is a commutative ring endowed with a morphism $A \to S^{-1}A$ such that for any other $A$-algebra $A \to B$ in which every element of $S$ becomes a unit, the map $A \to B$ factors through $S^{-1}A$ uniquely.

\begin{theorem}
  $S^{-1}A$ exists.
\end{theorem}
\begin{proof}
  We define $S^{-1}A$ as follows.
  Elements in $S^{-1}A$ are represented by pairs $(a,s)$, $a \in A$ and $s \in S$, where to pairs $(a,s)$ and $(b,t)$ are equivalent provided that there exists some $u \in S$ such that
  $$ a \cdot t \cdot u = b \cdot s \cdot u. $$
  \exercise{This is an equivalence relation.}
  The equivalence class of $a/s$ is denoted $\frac{a}{s}$.
  Sums and products are defined using the usual rules:
  $$ \frac{a}{s} + \frac{b}{t} = \frac{at+bs}{st}, \ \ \frac{a}{s} \cdot \frac{b}{t} = \frac{ab}{st}. $$
  The map $A \to S^{-1}A$ is given by $a \mapsto \frac{a}{1}$.
  We leave it as an exercise to check that this is indeed a ring, and that it satisfies the aforementioned universal property.
\end{proof}

\begin{remark}
  The equivalence relation mentioned in the above proof is the correct one, while the naive one where $(a,s) \sim (b,t)$ if and only if $a \cdot t = b \cdot s$ works in the case where $A$ is a domain and $S$ does not contain $0$.
\end{remark}

\begin{definition}
  Let $A$ be a ring.
  We use the following notations:
  \begin{enumerate}
  \item Given $f \in A$, we write $A_f = S^{-1}A$ where $S = \{1,f,f^2,\ldots\}$.
  \item Given $\pfrak$ a prime ideal in $A$, we write $A_\pfrak = S^{-1}A$ where $S = A \smin \pfrak$.
  \end{enumerate}
\end{definition}

\section{Expansion and Contration in localizations}

Let $A$ be a ring, $S$ a multiplicative set, and $\afrak$ a subset of $A$.
We define the \emph{saturation of $\afrak$ with respect to $S$}, denoted $\afrak^S$, as:
$$ \afrak^S := \{a \in A \ | \ \exists s \in S, \ a \cdot s \in \afrak \}. $$
If $\afrak = \afrak^S$, we say that $\afrak$ is saturated.

\begin{theorem}
  Assume that $S$ is as above and $\afrak$ an ideal of $A$.
  \begin{enumerate}
  \item The kernel of $A \to S^{-1}A$ is the saturation of $(0)$.
  \item One has $\afrak \subset \afrak^S$.
  \item $\afrak^S$ is an ideal of $A$.
  \end{enumerate}
\end{theorem}

\begin{theorem}
  The following hold:
  \begin{enumerate}
  \item Let $\bfrak$ be an ideal of $S^{-1}A$.
    Then $\bfrak \cap A$ is saturated and $\bfrak$ is the expansion of its contraction along $A \to S^{-1}A$.
  \item Let $\afrak$ be an ideal of $A$.
    Then $\afrak$ and its saturation have the same expansion to $S^{-1}A$, and the saturation of $\afrak$ is the contraction of its expansion to $S^{-1}A$.
  \item Let $\pfrak$ be a prime ideal of $A$ satisfying $\pfrak \cap S = \varnothing$.
    Then $\pfrak$ is saturated and its expansion to $S^{-1}A$ is prime.
  \end{enumerate}
\end{theorem}

\begin{corollary}
  Let $S$ be a multiplicative set in $A$.
  \begin{enumerate}
  \item The map $\afrak \mapsto \afrak \cdot S^{-1}A$ is an inclusion preserving bijection between the set of all saturated ideals in $A$ and the ideals of $S^{-1}A$.
    The inverse is contraction along $A \to S^{-1}A$.
  \item The map $\pfrak \to \pfrak \cdot S^{-1}A$ is an inclusion preserving bijection between the prime ideals of $A$ which are disjoint from $S$, and the primes of $S^{-1}A$.
    The inverse is contraction along $A \to S^{-1}A$.
  \end{enumerate}
\end{corollary}

\begin{corollary}
  $A_\pfrak$ is a local ring with maximal ideal $\pfrak \cdot A_\pfrak$.
\end{corollary}

\section{Localization of modules}

Let $A$ be a ring, $S$ a multiplicative subset, and $M$ an $A$-module.
We construct $S^{-1}M$ an $S^{-1}A$-module which initial with respect to all $S^{-1}A$-modules which are equipped with an $A$-linear map from $M$.
\todo{Write out universal property explicitly.}

\begin{theorem}
  $S^{-1}M$ exists.
\end{theorem}
\begin{proof}
  The construction is similar to the construction of $S^{-1}A$.
  Explicitly, elements of $S^{-1}M$ are represented by pairs $(m,s)$ with $m \in M$ and $s \in S$ subject to the equivalence relation
  $$ (m,s) \sim (m',s') $$
  if and only if there exists $t \in S$ such that $s'tm = stm'$.
  Equivalence classes are denoted by $\frac{m}{s}$.
  Addition is defined as usual:
  $$ \frac{m}{s} + \frac{m'}{s'} = \frac{s'm+sm'}{ss'} $$
  and the scalar multiplication of $S^{-1}A$ is defined as
  $$ \frac{a}{s} \cdot \frac{m}{t} = \frac{am}{st}. $$
  Again, the remaining details are left as an exercise.
\end{proof}

\begin{theorem}
  The functor $M \mapsto S^{-1}M$ is exact.
\end{theorem}
\begin{proof}
  The fact that it is right exact is simple.
  For left exactness, suppose that $f : M \to N$ is injective and that $\frac{m}{s} \in S^{-1}M$ maps to $0$ in $S^{-1}N$.
  I.e. we have
  $$ \frac{f(m)}{s} = \frac{0}{1} $$
  so that there exists some $t \in S$ such that $t \cdot f(m) = 0$.
  This means that $f(t \cdot m) = 0$ hence $t \cdot m = 0$ by the injectivity of $f$.
  But then it follows that $\frac{m}{t} = 0$.
\end{proof}

\begin{theorem}
  One has a canonical isomorphism
  $$ S^{-1}M \cong S^{-1}A \otimes_A M $$
  which is functorial in $M$.
  In particular, $S^{-1}A$ is a flat $A$-algebra, and localization of modules commutes with coproducts.
\end{theorem}
\begin{proof}
  The universal property tells us that $M \mapsto S^{-1}M$ is the left-adjoint to the restriction of scalars functor from the category of $S^{-1}A$-modules to $A$-modules.
  The same role is played by the tensor product mentioned in the statement of the theorem, and the uniqueness of adjoint functors completes the proof.
\end{proof}

\begin{lemma}
  Suppose that $M$ is finitely-generated and that $S^{-1}M = 0$.
  Then there exists $f \in S$ such that $M_f = 0$.
\end{lemma}
\begin{proof}
  Let $m_1,\ldots,m_n$ generate $M$.
  By assumption, for each $i$ there exists some $f_i \in S$ such that $f_i \cdot m_i = 0$.
  Take $f = f_1 \cdots f_n$.
\end{proof}

\begin{proposition}
  Let $M$ be an $A$-module, and $S$ a multiplicative subset of $A$.
  \begin{enumerate}
  \item Suppose that $m_1,\ldots,m_n \in M$ are given.
    If $M$ is finitely-generated, and the $m_i/1$ generated $S^{-1}M$ over $S^{-1}A$, then for some $f \in S$, the $m_i/1$ generate $M_f$ over $A_f$.
  \item Assume $M$ is finitely-presented and $S^{-1}M$ is free as a $S^{-1}A$-module with finite rank $n$.
    Then there is some $h \in S$ such that $M_h$ is free over $A_h$ with rank $n$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  For 1: Define $A^n \to M$ by sending $e_i$ to $m_i$, and let $C$ be the cokernel of this map.
  As localization is exact, it follows that the localization $S^{-1}C$ is the same as the cokernel of
  $$ (S^{-1}A)^n \to S^{-1}M. $$
  This is surjective by assumption so that $S^{-1}C = 0$.
  As $M$ is finitely-generated, the same holds for $C$, and the lemma above proves the claim \todo{add ref}. 

  For 2: Let $m_1/s_1,\ldots,m_n/s_n$ be a free basis of $S^{-1}M$ over $S^{-1}A$.
  Since $s_i$ are all units in $S^{-1}A$, the same holds for $m_1/1,\ldots,m_n/1$.
  Let $A^n \to M$ and $C$ be as above, and let $K$ be the kernel.
  Again, as localization is exact, we have
  $$ 0 \to S^{-1}K \to (S^{-1}A)^n \to S^{-1}M \to S^{-1}C \to 0 $$
  is exact with the middle arrow an isomorphism.
  By the lemma above, we find $f,g \in S$ such that $C_f = 0$ and $K_g = 0$.
  The assertion follows since $C_{fg} = 0$ and $K_{fg} = 0$.
\end{proof}

\begin{proposition}
  Let $S$ be a multiplicative subset of $A$, and $M$ and $N$ two $A$-modules.
  Then there is a canonical morphism
  $$ S^{-1} \Hom_A(M,N) \to \Hom_{S^{-1}A}(S^{-1}M,S^{-1}N). $$
  Furthermore, this map is injective if $M$ is finitely-generated, and it is an isomorphism if $M$ is finitely-presented.
\end{proposition}
\begin{proof}
  Put $B = S^{-1}A$.
  We first construct a map
  $$ B \otimes_A \Hom_A(M,N) \to \Hom_B(B \otimes_A M, B \otimes_A N). $$
  This map sends a basic tensor $b \otimes f$ to the map defined by
  $$ b' \otimes m \mapsto (bb') \otimes f(m). $$
  This map is easily checked to be an isomorphism if $M = A$, since $\Hom_A(A,N) = N$ and $B \otimes_A A = B$ and $\Hom_B(B,B \otimes_A N) = B \otimes_A N$ (the map becomes the identity on $B \otimes_A B$ with these identifications).
  Furthermore, this map is easily checked to be functorial in $M$.

  Suppose $M$ is finitely-generated then we have a short exact sequence of the form
  $$ A^{(S)} \to A^n \to M \to 0 $$
  with $S$ possibly infinite.
  Applying these functors, using the left exactness of Hom and the exactness of localization, we obtain a commutative diagram \todo{draw it!} with exact rows which is exact on the left and the middle arrow is an isomorphism.
  This shows injectivity in the case where $M$ is finitely-generated.
  If $M$ is finitely-presented, then we may take $S$ finite as well, so the right-hand map is also an isomorphism, hence the map under consideration is an isorophism.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
