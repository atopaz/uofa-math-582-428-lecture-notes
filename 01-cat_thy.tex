\chapter{Basics of Category Theory}

\section{Categories}

\subsection{Definitions}

\begin{definition}
  A category $\Cscr$ consists of the following data:
  \begin{enumerate}
  \item A \emph{collection} of objects, denoted by $\ob\Cscr$, or just $\Cscr$.
  \item For every pair of objects $X$, $Y$ of $\Cscr$, a \emph{collection} of arrows (or morphisms) $X \rightarrow Y$ denoted using one of the following symbols:
    \[ \Cscr(X,Y), \ \Hom_\Cscr(X,Y), \ \Hom^\Cscr(X,Y), \ \Hom(X,Y).  \]
  \item A composition rule for morphisms:
    \[ (f,g) \mapsto f \circ g \ : \  \Cscr(Y,Z) \times \Cscr(X,Y) \rightarrow \Cscr(X,Z).\]
  \end{enumerate}
  This data is subject to the following axioms:
  \begin{enumerate}
  \item Composition of morphisms is associative: $f \circ (g \circ h) = (f \circ g) \circ h$.
  \item For every object $X$, there exists an \emph{identity morphism} $\one_X$ which is both a left and right identity for function composition: $\one_X \circ g = g$ and $h \circ \one_X = h$.
  \end{enumerate}
\end{definition}

\begin{remark}
  Some remarks:
  \begin{enumerate}
  \item It follows from the axioms of a category that \exercise{identity morphisms are uniquely determined}.
  \item Often the collection of objects is a class and \emph{not a set}, while the collections of morphisms are assumed to be sets.
  \item A \emph{small category} is one where the collection of objects is a set, and all collections of morphisms are sets.
  \item A \emph{locally small category} $\Cscr$ is one where for every pair of objects $X,Y \in \Cscr$, the collection $\Cscr(X,Y)$ is a set.
  \end{enumerate}
\end{remark}

\begin{definition}
  A morphism $f : X \rightarrow Y$ in a category $\Cscr$ is called an \emph{isomorphism} provided that there exists a morphism $g : Y \rightarrow X$ satisfying:
  \[ g \circ f = \one_X, \ \ f \circ g = \one_Y. \]

\end{definition}

\subsection{Examples of Categories}

\begin{example}[Sets]
  The category of sets, denoted $\Set$, is the category whose objects are sets, and whose morphisms are functions between sets.
\end{example}

\begin{example}[Groups]
  The category of groups, denoted $\Group$, is the category whose objects are groups and whose morphisms are group homomorphisms.
\end{example}

\begin{example}[Rings]
  The category of rings, denoted $\Ring$, is the category whose objects are rings and whose morphisms are ring homomorphisms (see \S~\ref{subsection:rings_and_modules-definitions-rings}).
\end{example}

\begin{example}[Modules]
  Let $R$ be a ring.
  The category of $R$-modules, denoted $R\Mod$, is the category whose objects are $R$-modules and whose morphisms are homomorphisms of $R$-modules (see \S~\ref{section:rings_and_modules-definitions-modules}).
  The category of right $R$-modules, denoted $\Mod R$, is defined in the \exercise{analogous way}.
\end{example}

\begin{example}
  \exercise{The category of abelian groups (objects are abelian groups and morphisms are group homomorphisms) is precisely the category of $\Zbf$-modules.}
  We will sometimes denote this category by $\Ab$.
\end{example}

\begin{example}[Opposite Category]
  Let $\Cscr$ be a category.
  Then $\Cscr^\op$ is the category given by the following data:
  \begin{enumerate}
  \item $\ob\Cscr^\op = \ob\Cscr$.
  \item For all $X,Y \in \ob\Cscr = \ob\Cscr^\op$, one has $\Cscr^\op(X,Y) = \Cscr(Y,X)$.
  \item \exercise{Composition of morphisms is inherited from the composition law in $\Cscr$.}
  \end{enumerate}
  \exercise{It is easy to see that $\Cscr^\op$ is indeed a category.}
  If we wish to explicitly distinguish between objects/morphisms of $\Cscr$ resp. $\Cscr^\op$, we will write $X^\op$ for the object of $\Cscr^\op$ associated to $X \in \Cscr$, and/or $f^\op : Y^\op \rightarrow X^\op$ for the morphism in $\Cscr^\op$ associated to $f : X \rightarrow Y$ in $\Cscr$.
\end{example}

\begin{example}[Poset]
  Let $(S,\leq)$ be a partially ordered set.
  We can construct a (small) category $\Cscr_S$ as follows:
  \begin{enumerate}
  \item One has $\ob\Cscr_S := S$.
  \item For $s,t \in S = \ob\Cscr_S$, one has
    \[ \Hom(s,t) = \begin{cases}
        \{\star\} & s \leq t \\
        \varnothing & \text{otherwise}
      \end{cases}\]
  \end{enumerate}
  \exercise{It is easy to see that this forms a category.}
\end{example}

\begin{example}[Topological Spaces]
  The category of topological spaces, denoted $\Top$, is the category whose objects are topological spaces and whose morphisms are continuous maps between topological spaces.
\end{example}

\begin{example}[Group]
  Let $G$ be a group.
  We can view $G$ as a category with a single object $\star$ such that
  \[ \Hom(\star,\star) = G \]
  and for all $g,h \in \Hom(\star,\star) = G$, the composition $g \circ h$ is given by $g \cdot h$.
  \exercise{The group axioms ensure that all morphisms in this category are isomoprhisms.}
\end{example}

\begin{example}
  The category of categories, denoted $\Cat$, is the category whose objects are (small) categories and whose morphisms are functors between categories (see \S~\ref{subsection-cat_thy-functors-cat_of_functors}).
\end{example}

\begin{example}
  Let $\Cscr$ and $\Dscr$ be two categories.
  The \emph{product category} $\Cscr \times \Dscr$ is the category whose objects are pairs $(X,Y)$ where $X \in \Cscr$ and $Y \in \Dscr$, and where a morphism $(X,Y) \rightarrow (X',Y')$ consists of a pair $(f,g)$, where $f : X \rightarrow X'$ is a morphism in $\Cscr$ and $g : Y \rightarrow Y'$ is a morphism in $\Dscr$.
\end{example}

\section{Functors}

\subsection{Definitions}\label{subsection:cat_theory-functors-definitions}

\begin{definition}
  Let $\Cscr$ and $\Dscr$ be two categories.
  A \emph{functor} $\Fscr : \Cscr \rightarrow \Dscr$ is a rule which assigns an object $\Fscr(X) \in \Dscr$ for each object $X \in \Cscr$, and a morphism $\Fscr(f) : \Fscr(X) \rightarrow \Fscr(Y)$ for each morphism $f : X \rightarrow Y$ between two objects $X,Y \in \Cscr$, and which satisfies the following axioms:
  \begin{enumerate}
  \item For all $X \in \Cscr$, one has $\Fscr(\one_X) = \one_{\Fscr(X)}$.
  \item For all morphisms $g : X \rightarrow Y$ and $f : Y \rightarrow Z$ in $\Cscr$, one has $\Fscr(f \circ g) = \Fscr(f) \circ \Fscr(g)$. 
  \end{enumerate}
\end{definition}

\begin{definition}
  Let $\Cscr$ and $\Dscr$ be two categories.
  A \emph{presheaf} $\Fscr$ on $\Cscr$ with values in $\Dscr$, is a rule which assigns an object $\Fscr(X) \in \Dscr$ for each object $X \in \Cscr$, and a morphism $\Fscr(f) : \Fscr(Y) \rightarrow \Fscr(X)$ for each morphism $f : X \rightarrow Y$ between two objects $X,Y \in \Cscr$, and which satisfies the following axioms:
  \begin{enumerate}
  \item For all $X \in \Cscr$, one has $\Fscr(\one_X) = \one_{\Fscr(X)}$.
  \item For all morphisms $g : X \rightarrow Y$ and $f : Y \rightarrow Z$ in $\Cscr$, one has $\Fscr(f \circ g) = \Fscr(g) \circ \Fscr(f)$. 
  \end{enumerate}
  \exercise{There is a one-to-one correspondence between presheaves $\Cscr \rightarrow \Dscr$ and functors $\Cscr^\op \rightarrow \Dscr$.}
\end{definition}

\begin{remark}
  We will often identify the notion of a presheaf on $\Cscr$ with values in $\Dscr$ and that of a functor $\Cscr^\op \rightarrow \Dscr$.
\end{remark}

\subsection{Full, Faithful, and Essentially Surjective Functors}

\begin{definition}
  A functor $\Fscr : \Cscr \rightarrow \Dscr$ is called:
  \begin{enumerate}
  \item \emph{Full} if for all objects $X,Y \in \Cscr$, the map
    \[ \Fscr : \Hom(X,Y) \rightarrow \Hom(\Fscr(X),\Fscr(Y)) \]
    is \emph{surjective}.
  \item \emph{Faithful} if for all objects $X,Y \in \Cscr$, the map
    \[ \Fscr : \Hom(X,Y) \rightarrow \Hom(\Fscr(X),\Fscr(Y)) \]
    is \emph{injective}.
  \item \emph{Fully faithful} if it is full and faithful.
  \item \emph{Essentially surjective} if for all objects $Y \in \Dscr$ there exists some object $X \in \Cscr$ such that $Y$ is isomorphic to $\Fscr(X)$.
  \end{enumerate}
\end{definition}

\subsection{Examples of Functors}

\begin{example}[$h^X$]
  Let $\Cscr$ be a locally small category and let $X$ be an object in $\Cscr$.
  We may construct a functor $h^X : \Cscr \rightarrow \Set$ given by
  \[ h^X(Y) := \Hom(X,Y), \ \ h^X(f) := (g \mapsto f \circ g). \]
  Here $f : Y \rightarrow Z$ is an arbitrary morphism in $\Cscr$ and $g$ varies over the elements of $h^X(Y) = \Hom(X,Y)$ (so that $f \circ g$ is a morphism $X \rightarrow Z$, hence an element of $h^X(Z)$).
\end{example}

\begin{example}
  Let $\Cscr$ be a locally small category, and consider the category $\Fun(\Cscr,\Set)$ (see \S~\ref{subsection-cat_thy-functors-cat_of_functors}).
  The assignment
  \[ X \mapsto h^X \] 
  \exercise{extends to a functor} $h^- : \Cscr^\op \rightarrow \Fun(\Cscr,\Set)$.
  Given a morphism $f^\op : Y \rightarrow X$ in $\Cscr^\op$, corresponding to a morphism $f : X \rightarrow Y$ in $\Cscr$, the corresponding natural transformation $h^{f^\op}$ is given by
  \[ h^{f^\op}_Z : h^Y(Z) = \Hom(Y,Z) \rightarrow \Hom(X,Z) = h^X(Z), \ \ h \mapsto h \circ f. \]
\end{example}

\begin{example}
  Let $\Cscr$ be a locally small category and let $X$ be an object in $\Cscr$.
  We define $h_X$ to be the presheaf on $\Cscr$ with values in $\Set$ corresponding to the functor
  \[ h^X : \Cscr^\op \rightarrow \Set. \]
  \exercise{Explicitly, one has}
  \begin{enumerate}
  \item $h_X(Y) = \Hom(Y,X)$.
  \item If $f : Y \rightarrow Z$ is a morphism in $\Cscr$, then $h_X(f) : \Hom(Z,X) \rightarrow \Hom(Y,X)$ is the map $g \mapsto f \circ g$.
  \end{enumerate}
\end{example}

\begin{example}
  Let $\Cscr$ be a locally small category, and consider the category $\Fun(\Cscr^\op,\Set)$ (see \S~\ref{subsection-cat_thy-functors-cat_of_functors}).
  \exercise{The presheaf $h_-$ extends to a functor
  \[ h_- : \Cscr \rightarrow \Fun(\Cscr^\op,\Set). \]}
\end{example}

\begin{example}
  \exercise{
  Let $\Cscr$ be a locally small category, and consider the category $\Dscr := \Cscr^\op \times \Cscr$.
  Then
  \[ \Hom_\Cscr(-,-) \]
  can be considered as a functor $\Dscr \rightarrow \Set$.
  Explicitly, for an object $(X^\op,Y) \in \Dscr$ (i.e. $X,Y \in \Cscr$), this functor takes the value:
  \[ \Hom_\Cscr(X,Y) \]
  and a for a morphism $(f^\op,g) : (X,Y) \rightarrow (X',Y')$ in $\Dscr$ (so $f : X' \rightarrow X$ and $g : Y \rightarrow Y'$ are moprhisms in $\Cscr$), this functor takes the value;
  \[ \Hom_\Cscr(X,Y) \rightarrow \Hom_\Cscr(X',Y'), \ \ h \mapsto g \circ h \circ f. \]
  }
\end{example}

\subsection{The category of functors}\label{subsection-cat_thy-functors-cat_of_functors}

\begin{definition}
  Let $\Cscr$ and $\Dscr$ be two categories, and let $\Fscr, \Gscr : \Cscr \rightarrow \Dscr$ be two functors.
  A \emph{natural transformation} $\eta : \Fscr \rightarrow \Gscr$ is a rule which assigns to each object $X \in \Cscr$ a morphism
  \[ \eta_X : \Fscr(X) \rightarrow \Gscr(X) \] 
  satisfying the following axiom: For all morphisms $f : X \rightarrow Y$ in $\Cscr$, the following diagram commutes:
  \begin{center}
  \begin{tikzpicture}[node distance=3cm, auto]
    \node (FX) at (0,0) {$\Fscr(X)$};
    \node [right of=FX] (FY) {$\Fscr(Y)$};
    \node [below of=FX] (GX) {$\Gscr(X)$};
    \node [below of=FY] (GY) {$\Gscr(Y)$};
    \draw[->] (FX) to node {$\Fscr(f)$} (FY);
    \draw[->] (GX) to node [swap] {$\Gscr(f)$} (GY);
    \draw[->] (FX) to node [swap] {$\eta_X$} (GX);
    \draw[->] (FY) to node {$\eta_Y$} (GY);
  \end{tikzpicture}
  \end{center}
  in the category $\Dscr$.
  The collection of all natural transformations $\Fscr \rightarrow \Gscr$ will be denoted by one of the following:
  \[ \Hom(\Fscr,\Gscr), \ \Nat(\Fscr,\Gscr), \ [\Fscr,\Gscr]. \]
\end{definition}

\begin{remark}
  The collection of all functors $\Cscr \rightarrow \Dscr$ \exercise{forms a category} where morphisms are given by natural transformations of functors.
  This category is denoted as one of the following:
  \[ \Fun(\Cscr,\Dscr), \ \Hombf(\Cscr,\Dscr), \ [\Cscr,\Dscr], \ \Cscr^\Dscr. \]
  The bold $\Hombf$ is meant as a reminder that this is a category rather than just a collection.
\end{remark}

\subsection{Representable Functors}

\begin{definition}
  Let $\Cscr$ be a locally small category.
  A functor $\Fscr : \Cscr \rightarrow \Set$ is called \emph{representable} provided that $\Fscr$ is \exercise{isomorphic (in the category $[\Cscr,\Set]$)} to a functor of the form $h^X$ for some $X \in \Cscr$.
  A functor $\Gscr : \Cscr^\op \rightarrow \Set$ is called \emph{representable} provided that $\Gscr$ is \exercise{isomorphic (in the category $[\Cscr^\op,\Set]$)} to a functor of the form $h_X$ for some $X \in \Cscr$. 
\end{definition}

In certain situations we will want to keep track of the representing object $X$, and the universal element $x \in \Fscr(X)$ associated to a representable functor $\Fscr$.
In this case, we will say that $\Fscr$ is representable by $(X,x)$ (where $x \in \Fscr(X)$).
Explicitly, this means that there is an isomorphism of functors
\[ \eta : h^X \xto{\cong} \Fscr \]
such that $x = \eta_X(\one_X)$.
We may also leave $x$ implicit in certain cases, in which case we will say that $\Fscr$ is representable by $X$.

\begin{theorem}
  Let $\Cscr$ be a locally small category.
  \begin{enumerate}
  \item A functor $\Fscr : \Cscr \rightarrow \Set$ is representable if and only if there exists an object $X \in \Cscr$, and an element $x \in \Fscr(X)$ such that the pair $(X,x)$ is \emph{universal}, in the sense that whenever $(Y,y)$ is another pair with $Y \in \Cscr$ and $y \in \Fscr(y)$, there exists a \emph{unique} morphism $f : X \rightarrow Y$ in $\Cscr$ such that $y = \Fscr(f)(x)$.
  \item A functor $\Gscr : \Cscr^\op \rightarrow \Set$ is representable if and only if there exists an object $X \in \Cscr$, and an element $x \in \Gscr(X)$ such that the pair $(X,x)$ is \emph{universal}, in the sense that whenever $(Y,y)$ is another pair with $Y \in \Cscr$ and $y \in \Gscr(y)$, there exists a \emph{unique} morphism $f : Y \rightarrow X$ in $\Cscr$ such that $y = \Gscr(f)(x)$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \exercise{The proof is left as an exercise to the reader.}
\end{proof}

\subsection{Examples of Representable functors}

\begin{example}
  \exercise{Let $\CRing$ denote the category of commutative rings.
  The functor
  \[ \CRing \to \Set \]
  defined on objects by $A \mapsto \{\star\}$ is representable by $\Zbf$.}
\end{example}

\begin{example}
  \exercise{Let $\CRing$ denote the category of commutative rings.
  The forgetful functor 
  \[ \CRing \to \Set \]
  is representable by $\Zbf[X]$ (the polynomial ring in one variable over the ring of integers).}
\end{example}

\begin{example}
  \exercise{Let $\CRing$ denote the category of commutative rings.
    The functor
  \[ \CRing \to \Set \]
  defined on objects by $A \mapsto \{ a \in A \ | \ a^2 = 1 \}$ (and the obvious thing on morphisms) is representable by $\Zbf[X]/(X^2-1)$.
  }
\end{example}

\section{The Yoneda Lemma}

\begin{theorem}[Yoneda]
  Let $\Cscr$ be a locally small category, and let $X$ be an object of $\Cscr$.
  \begin{enumerate}
  \item Let $\Fscr : \Cscr \rightarrow \Set$ be a functor and consider $h^X : \Cscr \rightarrow \Set$.
    Then $[h^X,\Fscr] \cong \Fscr(X)$, and this isomorphism is natural in both $X$ and $\Fscr$.
  \item Let $\Fscr : \Cscr^\op \rightarrow \Set$ be a functor and consider $h_X : \Cscr^\op \rightarrow \Set$.
    Then $[h_X,\Fscr] \cong \Fscr(X)$, and this isomorphism is natural in both $X$ and $\Fscr$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  We will define a map $[h^X,\Fscr] \rightarrow \Fscr(X)$ and a map $\Fscr(X) \rightarrow [h^X,\Fscr]$.
  First, let $\eta \in [h^X,\Fscr]$ be given, and consider
  \[ \eta_X : h^X(X) = \Hom(X,X) \rightarrow \Fscr(X). \]
  The set $\Hom(X,X)$ has a distinguished element $\one_X$.
  We define the map $[h^X,\Fscr] \rightarrow \Fscr(X)$ by $\eta \mapsto \eta_X(\one_X)$.

  In the other direction, suppose that $c \in \Fscr(X)$ is given.
  We define $\eta : h^X \rightarrow \Fscr$ by
  \[ \eta_Y : h^X(Y) = \Hom(X,Y) \rightarrow \Fscr(Y) \]
  by $\eta_Y(g) := \Fscr(g)(c)$.
  \exercise{We leave it to the reader to check that these two maps are inverses of each-other, and that they are natural in $\Fscr$ and $X$.}
  This proves assertion 1.
  For assertion 2, we just replace $\Cscr$ by $\Cscr^\op$ (recall that $(\Cscr^\op)^\op = \Cscr$) and apply assertion 1.
\end{proof}

\section{Adjoint Functors}

\begin{definition}
  Let $\Cscr$ and $\Dscr$ be two (locally small) categories.
  An \emph{adjunction} between $\Cscr$ and $\Dscr$ is a pair of functors:
  \[ \Fscr : \Cscr \rightarrow \Dscr, \ \ \Gscr : \Dscr \rightarrow \Cscr \]
  and an isomorphism of \exercise{functors on $\Cscr^\op \times \Dscr$}:
  \[ \Phi \ : \ \Hom_\Dscr(\Fscr(-),-) \cong \Hom_\Cscr(-,\Gscr(-)).  \]
  In this case, we say that $\Fscr$ is left adjoint to $\Gscr$ and $\Gscr$ is right adjoint to $\Fscr$.
\end{definition}

\begin{theorem}
  Suppose that $\Fscr$ is a left adjoint to $\Gscr$.
  Then $\Gscr$ is uniquely determined upto natural isomorphism from $\Fscr$.
  Similarly, $\Fscr$ is uniquely determined upto natural isomorphism from $\Gscr$. 
\end{theorem}
\begin{proof}
  The proof follows easily from the Yoneda lemma.
  Indeed, suppose that $\Fscr$ is given.
  Consider the functor $\Hom(\Fscr(-),Y)$.
  \exercise{This is representable by assumption}, and the representing object (which is unique upto isomorphism) is what we define as $\Gscr(Y)$.
  If $Y \rightarrow Y'$ is a morphism, then we obtain a morphism of functors $\Hom(\Fscr(-),Y) \rightarrow \Hom(\Fscr(-),Y')$, which again defines a morphism $\Gscr(Y) \rightarrow \Gscr(Y')$ by the Yoneda lemma.
  \exercise{We leave the verification that this is indeed a functor (and a right adjoint to $\Fscr$), as well as the reconstruction of $\Fscr$ from $\Gscr$, to the reader.}
\end{proof}

\begin{lemma}\label{lemma-adjunction-unfold-naturality}
  Suppose that $\Fscr$ is adjoint to $\Gscr$ and that $\Phi : \Hom(\Fscr(-),-) \cong \Hom(-,\Gscr(-))$ is the adjunction isomorphism.
  Suppose that $f : X' \rightarrow X$ is a morphism, $g : \Fscr(X) \rightarrow Y$ is a morphism, and $h : Y \rightarrow Y'$ is a morphism.
  Then one has
  \[ h \circ g \circ \Fscr(f) : \Fscr(X') \rightarrow Y' \]
  and
  \[ \Gscr(h) \circ \Phi(g) \circ f : X' \rightarrow \Gscr(Y') \]
  are morphisms.
  The one has:
  \[ \Phi(h \circ g \circ \Fscr(f)) = \Gscr(h) \circ \Phi(g) \circ f. \]
\end{lemma}
\begin{proof}
  \exercise{This is merely a translation of the condition that $\Phi$ is a natural isomoprhism.}
\end{proof}

\section{Unit and Counit of an adjunction}

Let $\Fscr : \Cscr \rightarrow \Dscr$ and $\Gscr : \Dscr \rightarrow \Cscr$ be two functors.
Suppose that $\Fscr$ is a left adjoint of $\Gscr$.
Let $\Phi_{X,Y} : \Hom(\Fscr(X),Y) \cong \Hom(X,\Gscr(Y))$ be the isomorphism induced by the adjunction.
We will construct natural transformations:
\[ \eta : \Fscr \circ \Gscr \rightarrow \one_\Dscr, \ \epsilon : \one_\Cscr \rightarrow \Gscr \circ \Fscr. \]
First, $\eta_Y : \Fscr(\Gscr(Y)) \rightarrow Y$ is defined as $\Phi^{-1}_{\Gscr(Y),Y}(\one_{\Gscr Y})$.
Second, $\epsilon_X : X \rightarrow \Gscr(\Fscr(X))$ is defined as $\Phi_{X,\Fscr(X)}(\one_{\Fscr X})$.

\begin{theorem}
  In the above context, the compositions 
  \[ \Fscr \xrightarrow{\Fscr \epsilon} \Fscr \circ \Gscr \circ \Fscr \xrightarrow{\eta \Fscr} \Fscr \] 
  and
  \[ \Gscr \xrightarrow{\epsilon \Gscr} \Gscr \circ \Fscr \circ \Gscr \xrightarrow{\Gscr \eta} \Gscr \]
  are the identity on $\Fscr$ resp. $\Gscr$.
\end{theorem}
\begin{proof}
  This is easily checked from the definitions of $\epsilon$ and $\eta$, along with Lemma~\ref{lemma-adjunction-unfold-naturality}.
  \exercise{The details are left to the reader.}
\end{proof}

\begin{theorem}
  Suppose that $\Fscr : \Cscr \rightarrow \Dscr$ and $\Gscr : \Dscr \rightarrow \Cscr$ are two functors.
  Suppose we are given natural transformations 
  \[ \eta : \Fscr \circ \Gscr \rightarrow \one_\Dscr, \ \epsilon : \one_\Cscr \rightarrow \Gscr \circ \Fscr. \]
  such that the following compositions are the identity:
  \[ \Fscr \xrightarrow{\Fscr \eta} \Fscr \circ \Gscr \circ \Fscr \xrightarrow{\epsilon \Fscr} \Fscr. \] 
  \[ \Gscr \xrightarrow{\eta \Gscr} \Gscr \circ \Fscr \circ \Gscr \xrightarrow{\Gscr \epsilon} \Gscr. \]
  Then $\Fscr$ is a left adjoint to $\Gscr$.
\end{theorem}
\begin{proof}
  We must construct (functorial) isomorphisms
  \[ \Phi_{X,Y} : \Hom(\Fscr(X),Y) \cong \Hom(X,\Gscr(Y))\]
  for every $X \in \Cscr$ and $Y \in \Dscr$.
  Let $f \in \Hom(\Fscr(X),Y)$ be given.
  We define $\Phi_{X,Y}(f) = \Gscr(f) \circ \epsilon_X$.
  More explicitly, recall that $\epsilon_X$ is a morphism $X \rightarrow \Gscr(\Fscr(X))$, while $\Gscr(f)$ is a morphism $\Gscr(\Fscr(X)) \rightarrow \Gscr(Y)$, so they can indeed be composed.
  We may also define a map $\Psi$ in the other direction by sending $g : X \rightarrow \Gscr(Y)$ to $\eta_Y \circ \Fscr(g)$ (\exercise{it is easy to see that this is well-defined}).

  Let us check functoriality first.
  We have:
  \begin{align*}
    \Phi(h \circ g \circ \Fscr(f)) &= \Gscr(h \circ g \circ \Fscr(f)) \circ \epsilon_{X'} \\
                                         &= \Gscr(h) \circ \Gscr(g) \circ \Gscr(\Fscr(f)) \circ \epsilon_{X'} \\
                                         &= \Gscr(h) \circ \Gscr(g) \circ \epsilon_X \circ f \\
                                         &= \Gscr(g) \circ \Phi(g) \circ f
  \end{align*}
  To go from the second to third line, we used the naturality of $\epsilon$.

  We must check that $\Phi$ and $\Psi$ are inverses.
  \begin{align*}
    \Psi(\Phi(f)) &= \eta_Y \circ \Fscr(\Phi(f)) \\
                  &= \eta_Y \circ \Fscr(\Gscr(f) \circ \epsilon_X \\
                  &= f \circ \eta_{\Fscr X} \circ \Fscr(\epsilon_X) \\
                  &= f
  \end{align*}
  To go from the second to third line, we used the naturality of $\eta$, and to go from the third to the fourth line we used the assumption of the theorem.
  \exercise{The remaining cases (i.e. proving that $\Psi$ is natural, and that $\Phi \circ \Psi$ is the identity) follows similarly.}
\end{proof}

\subsection{Examples of Adjoint functors}

\begin{example}
  \exercise{
  Let $\Ab$ denote the category of abelian groups, and let $\Set$ denote the category of sets.
  Consider the \emph{forgetful functor}
  \[ \Fscr : \Ab \rightarrow \Set \]
  This functor has a left adjoint, $\Gscr : \Set \rightarrow \Fscr$, given on objects by
  \[ \Gscr(S) := \Zbf^{(S)} \]
  and for a function $f : S \rightarrow T$, one has
  \[ \Gscr(f) : \Zbf^{(S)} \rightarrow \Zbf^{(T)}, \ \ e_s \mapsto e_{f(s)}.  \]
  See \S~\ref{subsection:rings_and_modules-examples-modules} for the definitions.}
\end{example}

\begin{example}
  Let $\CRing$ denote the category of commutative rings, and let $\Set$ denote the category of sets.
  Consider the forgetful functor
  \[ \Fscr : \CRing \rightarrow \Set \]
  which assigns to each commutative ring (resp. morphism) the underlying set (resp. underlying function).
  \exercise{This functor has a left adjoint. }
\end{example}

\section{Products and Coproducts}

\begin{definition}
  Let $\Cscr$ be a category, and let $X_i$, $i \in I$ be a family of objects of $\Cscr$ indexed by some set $I$.
  We say that the {product of $(X_i)_i$} exists in $\Cscr$ provided that the functor
  \[ Y \mapsto \prod_{i \in I} \Hom(Y,X_i) \]
  is representable.
  The representing object (which is unique upto a unique isomorphism) is denoted by $\prod_{i \in I} X_i$. 
  \exercise{Compare this with the ``usual'' definition of the product in a category as found on Wikipedia.}
\end{definition}

\begin{definition}
  Let $\Cscr$ be a category, and let $X_i$, $i \in I$ be a family of objects of $\Cscr$ indexed by some set $I$.
  We say that the {coproduct of $(X_i)_i$} exists in $\Cscr$ provided that the functor
  \[ Y \mapsto \prod_{i \in I} \Hom(X_i,Y) \]
  is representable.
  The representing object (which is unique upto a unique isomorphism) is denoted by $\prod_{i \in I} X_i$. 
  \exercise{Compare this with the ``usual'' definition of the coproduct in a category as found on Wikipedia.}
\end{definition}

\begin{example}
  The category of sets has all products and coproducts.
  The product corresponds to the Cartesian product, and the coproduct corresponds to the disjoint union.
\end{example}

\section{Equivalences of Categories}

\begin{definition}
  Let $\Cscr$ and $\Dscr$ be two categories.
  An equivalence between $\Cscr$ and $\Dscr$ is a pair of functors $\Fscr : \Cscr \to \Dscr$ and $\Gscr : \Dscr \to \Fscr$ such that $\Fscr \circ \Gscr \cong \one_\Dscr$ and $\Gscr \circ \Fscr \cong \one_\Cscr$.
\end{definition}

Note that an isomorphism between categories is an equivalence, but the notion of equivalence is much more flexible.

\begin{definition}
  A functor $\Fscr : \Cscr \to \Dscr$ is essentially surjective provided that every object $Y$ of $\Dscr$ is isomorphic to $\Fscr(X)$ for some object $X$ of $\Cscr$.
\end{definition}

\begin{theorem}
  Let $\Fscr : \Cscr \to \Dscr$ be a functor.
  The following are equivalent:
  \begin{enumerate}
  \item The functor $\Fscr$ is fully faithful and essentially surjective.
  \item $\Fscr$ is an equivalence.
  \end{enumerate}
\end{theorem}

\begin{definition}
  Let $\Cscr$ be a category and $S$ a subcollection of objects of $\Cscr$.
  The \emph{full subcategory associated to $S$} is the category whose objects are those in $S$, and whose morphism collection $\Hom(X,Y)$, for $X,Y \in S$, is given by $\Hom^\Cscr(X,Y)$.
\end{definition}

\begin{theorem}
  Let $\Fscr$ be a fully faithful functor.
  Then $\Fscr$ is equivalent to the full subcategory associated to $\Fscr(\Cscr)$.
\end{theorem}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
