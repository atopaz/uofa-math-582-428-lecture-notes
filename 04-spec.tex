\chapter{Spec}

\section{Definition}

Let $A$ be a commutative ring.
We write $\Spec(A)$ for the set of prime ideals of $A$. 

Let $\afrak$ be an ideal of $A$, and define
$$ \Vbf(\afrak) = \{ \pfrak \in \Spec(A) \ | \ \afrak \subset \pfrak \}. $$

\begin{theorem}
  The following hold:
  \begin{enumerate}
  \item For $\afrak_1,\ldots,\afrak_n$ finitely many ideals of $A$, one has
    $$ \Vbf(\afrak_1) \cup \cdots \cup \Vbf(\afrak_n) = \Vbf(\afrak_1 \cap \cdots \cap \afrak_n) $$
    and 
    $$ \Vbf(\afrak_1) \cup \cdots \cup \Vbf(\afrak_n) = \Vbf(\afrak_1 \cdots \afrak_n) $$
  \item If $\afrak_i$, $i \in I$ is an arbitrary collection of ideals of $A$, then one has
    $$ \bigcap_i \Vbf(\afrak_i) = \Vbf(\sum_i \afrak_i). $$
  \item One has $\Vbf(1) = \varnothing$ and $\Vbf(0) = \Spec(A)$.
  \end{enumerate}
  In particular, as $\afrak$ varies over all ideals of $A$, the sets $\Vbf(\afrak)$ satisfy the axioms for closed sets of a topology on $\Spec(A)$.
\end{theorem}

For $f \in A$, define
$$ \Dbf(f) = \Spec(A) \smin \Vbf(f). $$
I.e. $\Dbf(f)$ is the set of prime ideals of $A$ which do not contain $f$.
A set of the form $\Dbf(f)$ will be called a \emph{principal open set} of $\Spec(A)$.
\begin{theorem}
  The $\Dbf(f)$ form a basis for the Zariski topology of $\Spec(A)$.
\end{theorem}
\begin{proof}
  If $\afrak$ is not contained in a prime $\pfrak$, then there is some $f \in \afrak \smin \pfrak$ so that $\pfrak \in \Dbf(f) \subset \Spec(A) \smin \Vbf(\afrak)$.
  Furthermore, $f,g \notin \pfrak$ if and only if $f \cdot g \notin \pfrak$ for a prime $\pfrak$.
  Thus,
  $$ \Dbf(f) \cap \Dbf(g) = \Dbf(f \cdot g). $$
\end{proof}

\begin{theorem}
  $\Spec(A)$ is quasi-compact, i.e. any open cover of $\Spec(A)$ has a finite subcover.
\end{theorem}

\begin{remark}
  A topological space is called \emph{compact} provided that it is both Hausdorff and quasi-compact.
  \exercise{$\Spec(A)$ is almost never Hausdorff.}
\end{remark}

\section{Functoriality}

Let $f : A \to B$ be a morphism of rings.
Given a prime $\pfrak$ of $B$, the contraction $\pfrak \cap A$ is a prime ideal in $A$.
This defined a map
$$ \Spec(f) : \Spec(B) \to \Spec(A). $$

\begin{theorem}
  In the above context, the map 
  $$ \Spec(f) : \Spec(B) \to \Spec(A) $$
  is continuous.
  The rule $\Spec(-)$ defined a contravariant functor from the category of commutative rings to the category of topological spaces.
\end{theorem}
\begin{proof}
  Easy, since
  $$ \Spec(f)^{-1}(\Vbf(\afrak)) = \Vbf(\afrak \cdot B). $$
  Alternatively,
  $$ \Spec(f)^{-1}(\Dbf(a)) = \Dbf(f(a)). $$
\end{proof}

\begin{theorem}
  The localization $A \to A_f$ induces a homeomorphism
  $$ \Spec A_f \to \Dbf(f). $$
\end{theorem}

\begin{theorem}
  Let $\afrak$ be an ideal in $A$.
  The quotient map
  $$ A \to A/\afrak $$
  induces a homeomorphism
  $$ \Spec A/\afrak \to \Vbf(\afrak). $$
\end{theorem}

\section{Structure of closed sets}


\begin{lemma}
  Let $\afrak$ and $\bfrak$ be two ideals.
  Then $\Vbf(\afrak) = \Vbf(\bfrak)$ if and only if $\sqrt{\afrak} = \sqrt{\bfrak}$.
\end{lemma}

\begin{definition}
  An ideal $\afrak$ is called \emph{radical} provided that $\sqrt{\afrak} = \afrak$.
\end{definition}

\begin{theorem}
  There is a canonical bijection between the following two sets:
  \begin{enumerate}
  \item The set of all \emph{radical} ideals $\afrak$ of $A$.
  \item The collection of closed sets of $\Spec A$.
  \end{enumerate}
  The bijection sends a radical ideal $\afrak$ to $\Vbf(\afrak)$
\end{theorem}

\section{Support}

Let $M$ be an $A$-module.
The \emph{support of $M$} is the following subset of $\Spec A$:
$$ \Supp M := \{ \pfrak \in \Spec A \ | \ M_\pfrak \neq 0 \}. $$
\begin{theorem}
  The following hold:
  \begin{enumerate}
  \item Suppose
    $$ 0 \to M \to N \to L \to 0 $$
    is exact.
    Then $\Supp N = \Supp M \cup \Supp L$.
  \item Suppose $M = \sum_i M_i$.
    Then $\Supp M = \bigcup_i \Supp M_i$.
  \item One has $\Supp M \subset \Vbf(\Ann(M))$, with equality if $M$ is finitely-generated.
  \item $\Rad(M)$ is contained in the intersection of all maximal ideals in $\Supp M$, with equality if $M$ is finitely-generated.
  \item One has $\Supp M \otimes_A N \subset \Supp M \cap \Supp N$ with equality if $M$ and $N$ are finitely-generated.
  \end{enumerate}
\end{theorem}

\begin{lemma}
  Let $M$ be a module.
  The following are equivalent:
  \begin{enumerate}
  \item $M = 0$.
  \item For all prime ideals $\pfrak$, one has $M_\pfrak = 0$.
  \item For all maximal ideals $\mfrak$, one has $M_\mfrak = 0$.
  \end{enumerate}
\end{lemma}

\begin{lemma}
  Let $M \to N \to L$ be a sequence of modules.
  The following are equivalent:
  \begin{enumerate}
  \item The sequence $M \to N \to L$ is exact.
  \item For every prime $\pfrak$, the localized sequence $M_\pfrak \to N_\pfrak \to L_\pfrak$ is exact.
  \item For every maximal ideal $\mfrak$, the localized sequence $M_\mfrak \to N_\mfrak \to L_\mfrak$ is exact.
  \end{enumerate}
\end{lemma}

\section{Local nature of flatness}

\begin{lemma}
  Given a module $M$, the following are equivalent:
  \begin{enumerate}
  \item $M$ is flat.
  \item $M_\pfrak$ is flat over $A_\pfrak$ for every prime $\pfrak$ of $A$.
  \item $M_\mfrak$ is flat over $A_\mfrak$ for every maximal ideal $\mfrak$ of $A$.
  \end{enumerate}
\end{lemma}

\section{Local conditions}

Let $M$ be a module.
We say that $M$ is \emph{locally finitely-generated} if for every prime $\pfrak$ of $A$, there is an open neighborhood of $\pfrak$ on which $M$ becomes finitely-generated.
In other words, for every prime $\pfrak$, there exists an element $f \in A \smin \pfrak$ such that $M_f$ is finitely-generated over $A_f$.
It is actually enough to check this condition for every maximal ideal $\mfrak$, since any prime $\pfrak$ lies in some maximal ideal $\mfrak$.
We define the following concepts analogously:
\begin{enumerate}
\item Locally finitely-presented modules.
\item Locally free modules (of finite rank).
\item Locally free modules of a fixed rank $n$.
\end{enumerate}

\begin{theorem}
  Let $M$ be a module.
  The following are equivalent:
  \begin{enumerate}
  \item $M$ is locally finitely-generated.
  \item $M$ is finitely-generated.
  \end{enumerate}
  A similar statement holds for (locally) finitely-presented modules.
\end{theorem}

\begin{theorem}
  Let $M$ be a module.
  The following are equivalent:
  \begin{enumerate}
  \item $M$ is finitely-generated and projective.
  \item $M$ is finitely-presented and flat.
  \item $M$ is finitely-presented, and $M_\mfrak$ is free over $A_\mfrak$ for every maximal ideal $\mfrak$.
  \item $M$ is locally free of finite rank.
  \item $M$ is finitely-generated, and for every prime $\pfrak$, there exists some $f \in A$ and some nonnegative integer $n$ (which might depend on $\pfrak$) such that $f \in \Dbf(f)$ and $M_{\pfrak'}$ is free of rank $n$ over $A_{\pfrak'}$ for every $\pfrak' \in \Dbf(f)$.
  \end{enumerate}
\end{theorem}

\section{Faithful Flatness}

Let $M$ be a module.
We say that $M$ is \emph{faithfully flat} provided that
\begin{enumerate}
\item $M$ is flat and
\item Whenever $N$ is an $A$-module, $M \otimes_A N = 0$ if and only if $N = 0$.
\end{enumerate}


\begin{proposition}
  Let $M$ be a flat $A$-module.
  The following are equivalent:
  \begin{enumerate}
  \item $M$ is faithfully flat.
  \item If $f : N \to N'$ is a morphism of modules, then $f$ is an isomorphism if and only if $f : M \otimes_A N \to M \otimes_A N'$ is an isomorphism.
  \item For maximal ideals $\mfrak$ of $A$, one has $M \neq \mfrak M$.
  \end{enumerate}
\end{proposition}

\begin{theorem}
  Let $f : A \to B$ be a flat $A$-algebra.
  The following are equivalent
  \begin{enumerate}
  \item $f$ is faithfully flat.
  \item For every prime $\pfrak$ of $A$, there exists a prime $\qfrak$ of $B$ such that $\qfrak \cap A = \pfrak$.
    I.e. the map $\Spec(f) : \Spec B \to \Spec A$ is surjective.
  \item For every maximal ideal $\mfrak$ of $A$, there exists a maximal ideal $\mathfrak{M}$ of $B$ such that $\mathfrak{M} \cap A = \mfrak$.
  \end{enumerate}
\end{theorem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
